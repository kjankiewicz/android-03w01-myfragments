package com.example.kjankiewicz.android_03w01

import android.support.v4.app.FragmentManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.FrameLayout
import android.widget.LinearLayout

/*
 * Aktywność implementuje odbiornik ContinentSelectionListener
 * Komunikacja pomiędzy fragmentem a aktywnością
 * Android-03w-interfejs-cz1: slajd 10
 */
class MyMainActivity : AppCompatActivity(),
        MyContinentsFragment.ContinentSelectionListener {

    private var mCitiesFragment: MyCitiesFragment? = null
    private lateinit var mContinentsFragment: MyContinentsFragment
    private lateinit var mFragmentManager: FragmentManager
    private lateinit var mContinentsFrameLayout: FrameLayout
    private lateinit var mCitiesFrameLayout: FrameLayout

    private var currentIndex = -1

    override fun onCreate(savedInstanceState: Bundle?) {

        Log.i(TAG, "onCreate()")

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_my_main)

        mContinentsFrameLayout = findViewById(R.id.continents_fragment_container)
        mCitiesFrameLayout = findViewById(R.id.cities_fragment_container)

        mFragmentManager = supportFragmentManager

        mFragmentManager.addOnBackStackChangedListener{ this.setLayout() }

        if (savedInstanceState == null) {
            /*
             * Dynamiczne dołączanie fragmentów do aktywności
             * Android-03w-interfejs-cz1: slajd 8 i 9
             */
            val fragmentTransaction = mFragmentManager.beginTransaction()
            mContinentsFragment = MyContinentsFragment()
            fragmentTransaction.add(R.id.continents_fragment_container, mContinentsFragment)
            fragmentTransaction.commit()
        } else {
            currentIndex = savedInstanceState.getInt(CURRENT_INDEX)
            if (currentIndex >= 0) onListSelection(currentIndex)
        }
    }

    private fun setLayout() {
        if (mCitiesFragment == null || !mCitiesFragment!!.isAdded) {
            mContinentsFrameLayout.layoutParams =
                    LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            mCitiesFrameLayout.layoutParams =
                    LinearLayout.LayoutParams(0, MATCH_PARENT)
        } else {
            mContinentsFrameLayout.layoutParams =
                    LinearLayout.LayoutParams(0, MATCH_PARENT, 1f)
            mCitiesFrameLayout.layoutParams =
                    LinearLayout.LayoutParams(0, MATCH_PARENT, 2f)
        }
    }

    /*
     * Implementacja metody z interfejsu odbiornika ContinentSelectionListener
     * Komunikacja pomiędzy fragmentem a aktywnością
     * Android-03w-interfejs-cz1: slajd 10
     */
    override fun onListSelection(index: Int) {
        currentIndex = index
        if (mCitiesFragment == null) {
            mCitiesFragment = MyCitiesFragment()
        }
        mCitiesFragment?.let {
            if (!it.isAdded) {
                val fragmentTransaction =
                        mFragmentManager.beginTransaction()
                fragmentTransaction.add(R.id.cities_fragment_container, it)
                fragmentTransaction.addToBackStack(null)

                fragmentTransaction.commit()
                mFragmentManager.executePendingTransactions()
            }
            it.showIndex(index)
        }
    }

    override fun onDestroy() {
        Log.i(TAG, "onDestroy()")
        super.onDestroy()
    }

    override fun onPause() {
        Log.i(TAG, "onPause()")

        mCitiesFragment?.let {
            if (it.isAdded) {
                val fragmentTransaction = mFragmentManager.beginTransaction()
                mFragmentManager.popBackStack()
                fragmentTransaction.remove(it)
                fragmentTransaction.commit()
                mFragmentManager.executePendingTransactions()
            } else
                currentIndex = -1
        }
        if (mCitiesFragment == null) currentIndex = -1
        super.onPause()
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putInt(CURRENT_INDEX, currentIndex)
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onRestart() {
        Log.i(TAG, "onRestart()")
        super.onRestart()
    }

    override fun onResume() {
        Log.i(TAG, "onResume()")
        super.onResume()
    }

    override fun onStart() {
        Log.i(TAG, "onStart()")
        super.onStart()
    }

    override fun onStop() {
        Log.i(TAG, "onStop()")
        super.onStop()
    }

    companion object {
        private const val MATCH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT
        private const val CURRENT_INDEX = "CURRENT_INDEX"
        private const val TAG = "MainActivityTag"
    }
}