package com.example.kjankiewicz.android_03w01

import android.content.Context
import android.support.v4.app.ListFragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter

/*
 * Fragment rozszerzający klasę ListFragment
 * Różne typy fragmentów
 * Android-03w-interfejs-cz1: slajd 13
 */
class MyCitiesFragment : ListFragment() {

    private lateinit var mAsiaArrayAdapter: ArrayAdapter<String>
    private lateinit var mEuropeArrayAdapter: ArrayAdapter<String>
    private lateinit var mAmericaArrayAdapter: ArrayAdapter<String>

    private lateinit var mAsiaArray: Array<String>
    private lateinit var mEuropeArray: Array<String>
    private lateinit var mAmericaArray: Array<String>

    override fun onAttach(context: Context) {
        Log.i(TAG, "onAttach()")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "onCreate()")
        super.onCreate(savedInstanceState)
        mAsiaArray = resources.getStringArray(R.array.Asia)
        mEuropeArray = resources.getStringArray(R.array.Europe)
        mAmericaArray = resources.getStringArray(R.array.America)
        context?.let {
            mAmericaArrayAdapter = ArrayAdapter(it,
                    R.layout.city_item, mAmericaArray)
            mEuropeArrayAdapter = ArrayAdapter(it,
                    R.layout.city_item, mEuropeArray)
            mAsiaArrayAdapter = ArrayAdapter(it,
                    R.layout.city_item, mAsiaArray)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.i(TAG, "onCreateView()")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.i(TAG, "onActivityCreated()")
        super.onActivityCreated(savedInstanceState)

    }

    fun showIndex(index: Int) {
        listAdapter = when (index) {
            0 -> mAmericaArrayAdapter
            1 -> mEuropeArrayAdapter
            else -> mAsiaArrayAdapter
        }
    }

    override fun onStart() {
        Log.i(TAG, "onStart()")
        super.onStart()
    }

    override fun onResume() {
        Log.i(TAG, "onResume()")
        super.onResume()
    }


    override fun onPause() {
        Log.i(TAG, "onPause()")
        super.onPause()
    }

    override fun onStop() {
        Log.i(TAG, "onStop()")
        super.onStop()
    }

    override fun onDetach() {
        Log.i(TAG, "onDetach()")
        super.onDetach()
    }


    override fun onDestroy() {
        Log.i(TAG, "onDestroy()")
        super.onDestroy()
    }

    override fun onDestroyView() {
        Log.i(TAG, "onDestroyView()")
        super.onDestroyView()
    }

    companion object {
        private const val TAG = "CitiesFragmentTag"
    }

}
