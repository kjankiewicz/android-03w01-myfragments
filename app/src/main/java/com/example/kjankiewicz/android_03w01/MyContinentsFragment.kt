package com.example.kjankiewicz.android_03w01

import android.content.Context
import android.support.v4.app.ListFragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView

/*
 * Fragment rozszerzający klasę ListFragment
 * Różne typy fragmentów
 * Android-03w-interfejs-cz1: slajd 13
 */
class MyContinentsFragment : ListFragment() {

    private lateinit var mContinentsArray: Array<String>
    private lateinit var mListener: ContinentSelectionListener
    private lateinit var mContinentsArrayAdapter: ArrayAdapter<String>

    interface ContinentSelectionListener {
        fun onListSelection(index: Int)
    }

    override fun onListItemClick(l: ListView, v: View, pos: Int, id: Long) {
        listView.setItemChecked(pos, true)
        mListener.onListSelection(pos)
    }

    override fun onAttach(context: Context) {
        Log.i(TAG, "onAttach()")
        super.onAttach(context)

        try {
            mListener = context as ContinentSelectionListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must " +
                    "implement ContinentSelectionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "onCreate()")
        retainInstance = true
        mContinentsArray = resources.getStringArray(R.array.Continents)
        context?.let {
            mContinentsArrayAdapter = ArrayAdapter(it,
                    R.layout.continent_item, mContinentsArray)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        Log.i(TAG, "onCreateView()")
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    /*
     * Cykl życia fragmentu
     * Metoda wywoływana, gdy aktywność zawierająca
     * fragment zakończyła realizację onCreate
     * Android-03w-interfejs-cz1: slajdy 5 i 6
     */
    override fun onActivityCreated(savedState: Bundle?) {
        Log.i(TAG, "onActivityCreated()")
        super.onActivityCreated(savedState)
        listAdapter = mContinentsArrayAdapter
        listView.choiceMode = ListView.CHOICE_MODE_SINGLE
    }

    override fun onStart() {
        Log.i(TAG, "onStart()")
        super.onStart()
    }

    override fun onResume() {
        Log.i(TAG, "onResume()")
        super.onResume()
    }

    override fun onPause() {
        Log.i(TAG, "onPause()")
        super.onPause()
    }

    override fun onStop() {
        Log.i(TAG, "onStop()")
        super.onStop()
    }

    override fun onDetach() {
        Log.i(TAG, "onDetach()")
        super.onDetach()
    }

    override fun onDestroy() {
        Log.i(TAG, "onDestroy()")
        super.onDestroy()
    }

    override fun onDestroyView() {
        Log.i(TAG, "onDestroyView()")
        super.onDestroyView()
    }

    companion object {
        private const val TAG = "ContinentsFragmentTag"
    }

}
