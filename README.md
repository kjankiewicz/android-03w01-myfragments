Tytuł: **Wykorzystanie Fragmentów w aplikacji Android**

Zaprezentowane zagadnienia: 

* Cykl życia Fragmentów
* Dynamiczne dołączanie fragmentów do aktywności 
* Komunikacja pomiędzy fragmentem a aktywnością
* Różne typy fragmentów 
* Dodawanie fragmentów na stos wywołań

Projekt prezentuje wykorzystanie Fragmentów. Główna aktywność wyświetla listę kontynentów i miast na nich się znajdujących za pomocą dwóch klas *ListFragment* oraz obsługuje komunikację z fragmentem wyświetlającym kontynenty. 

Najważniejsze pliki: 

* *MyMainActivity.java* - główna aktywność aplikacji dynamicznie dołączająca fragmenty i komunikująca się z fragmentem *MyContinentsFragment* przez odbiornik *ContinentSelectionListener* oraz obsługująca dodawanie fragmentów na stos wywołań 
* *MyContinentsFragment.java* - Fragment rozszerzający klasę *ListFragment*, wyświetlający listę kontynentów i komunikujący się z aktywnością *MyMainActivity*
* *MyCitiesFragment.java* - Fragment rozszerzający klasę *ListFragment*, wyświetlający listę miast znajdujących się na wybranym kontynencie.